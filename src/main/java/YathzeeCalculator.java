import java.util.*;

public class YathzeeCalculator {


    public static int getValueForNumbers(List<Integer> des, int number) {
        int result =0;
        for(int d : des){
            if(d == number){
                result+=d;
            }
        }
        return result;
    }

    public static int sumOfFirstSection(List<Integer> values) {
        int sum =0;
        for(int d : values){
            sum+=d;
        }
        return sum;
    }

    public static int bonus(int sum) {
        if(sum >= 63){
            return 35;
        }
        return 0;
    }

    public static int getThreeOfAKind(List<Integer> des) {
        int result = 0;
        boolean haveThreeOfAKind = false;
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int d : des){
            result+=d;
            if(!map.containsKey(d)){
                map.put(d,1);
            }else{
                map.put(d, map.get(d)+1);
                if(map.get(d) >=3) haveThreeOfAKind=true;
            }
        }
        if(haveThreeOfAKind) return result;
        return 0;
    }
    public static int getFourOfAKind(List<Integer> des) {
        int result = 0;
        boolean haveFourOfAKind = false;
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int d : des){
            result+=d;
            if(!map.containsKey(d)){
                map.put(d,1);
            }else{
                map.put(d, map.get(d)+1);
                if(map.get(d) >=4) haveFourOfAKind=true;
            }
        }
        if(haveFourOfAKind) return result;
        return 0;
    }

    public static int getFullHouse(List<Integer> des) {
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int d : des){
            if(!map.containsKey(d)){
                map.put(d,1);
            }else{
                map.put(d, map.get(d)+1);
            }
        }
        if(map.size()==2) return 25;
        return 0;
    }

    public static int getSmallStraight(List<Integer> des) {

        int i = 0;
        List<Integer> list = des;
        Collections.sort(list);
        Set<Integer> uniqueList = new HashSet<Integer>(list);
        int value = uniqueList.iterator().next();

        for(int d : uniqueList){
            if(d==value){
                value++;
                i++;
            }else if(i == 4){
                return 30;
            }
            else{
                return 0;
            }
        }
        return -1;
    }

    public static int getLargeStraight(List<Integer> des) {

        List<Integer> list = des;
        Collections.sort(list);
        Set<Integer> uniqueList = new HashSet<Integer>(list);
        int value = uniqueList.iterator().next();

        for(int d : uniqueList){
            if(d==value){
                value++;
            }else{
                return 0;
            }
        }
        return 40;
    }

}


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

public class YathzeeTest {
    List<Integer> des;

    @Test
    public void test_one() throws Exception {
        des = new ArrayList<>(Arrays.asList(1,1,1,4,6));
        assertEquals(3, YathzeeCalculator.getValueForNumbers(des,1));
    }

    @Test
    public void test_twos() throws Exception {
        des = new ArrayList<>(Arrays.asList(1,2,1,4,2));
        assertEquals(4, YathzeeCalculator.getValueForNumbers(des,2));
    }

    @Test
    public void test_threes() throws Exception {
        des = new ArrayList<>(Arrays.asList(1,3,1,4,6));
        assertEquals(3, YathzeeCalculator.getValueForNumbers(des,3));
    }

    @Test
    public void test_fours() throws Exception {
        des = new ArrayList<>(Arrays.asList(4,2,4,4,4));
        assertEquals(16, YathzeeCalculator.getValueForNumbers(des,4));
    }

    @Test
    public void test_fives() throws Exception {
        des = new ArrayList<>(Arrays.asList(5,3,5,4,5));
        assertEquals(15, YathzeeCalculator.getValueForNumbers(des,5));
    }

    @Test
    public void test_sixs() throws Exception {
        des = new ArrayList<>(Arrays.asList(4,2,6,4,6));
        assertEquals(12, YathzeeCalculator.getValueForNumbers(des,6));
    }

    @Test
    public void testSum() throws Exception{
        List<Integer> values = new ArrayList<>(Arrays.asList(0,4,3,8,10,12));
        assertEquals(37, YathzeeCalculator.sumOfFirstSection(values));
    }

    @Test
    public void testBonus() throws Exception{
        assertEquals(35, YathzeeCalculator.bonus(65));
    }

    @Test
    public void testThreeOfAKind() throws Exception {
        des = new ArrayList<>(Arrays.asList(3,3,1,4,3));
        assertEquals(14, YathzeeCalculator.getThreeOfAKind(des));
        des = new ArrayList<>(Arrays.asList(3,4,1,4,3));
        assertEquals(0, YathzeeCalculator.getThreeOfAKind(des));
    }

    @Test
    public void testFourOfAKind() throws Exception {
        des = new ArrayList<>(Arrays.asList(3,3,4,3,3));
        assertEquals(16, YathzeeCalculator.getFourOfAKind(des));
        des = new ArrayList<>(Arrays.asList(3,4,1,4,3));
        assertEquals(0, YathzeeCalculator.getFourOfAKind(des));
    }

    @Test
    public void testFullHouse() throws Exception {
        des = new ArrayList<>(Arrays.asList(3,4,4,3,3));
        assertEquals(25, YathzeeCalculator.getFullHouse(des));
        des = new ArrayList<>(Arrays.asList(3,2,1,4,3));
        assertEquals(0, YathzeeCalculator.getFullHouse(des));
    }

    @Test
    public void testSmallStraight() throws Exception {
        des = new ArrayList<>(Arrays.asList(1,3,4,2,6));
        assertEquals(30, YathzeeCalculator.getSmallStraight(des));
        des = new ArrayList<>(Arrays.asList(2,2,5,4,5));
        assertEquals(0, YathzeeCalculator.getSmallStraight(des));
    }

    @Test
    public void testLargeStraight() throws Exception {
        des = new ArrayList<>(Arrays.asList(1,3,4,2,5));
        assertEquals(40, YathzeeCalculator.getLargeStraight(des));
        des = new ArrayList<>(Arrays.asList(2,2,5,4,5));
        assertEquals(0, YathzeeCalculator.getLargeStraight(des));
    }

}
